package ru.t1.avfilippov.tm;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.client.ProjectRestEndpointClient;
import ru.t1.avfilippov.tm.dto.ProjectDto;
import ru.t1.avfilippov.tm.marker.IntegrationCategory;

import java.util.List;
import java.util.Objects;

public class ProjectRestEndpointTest {

    final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();

    final ProjectDto projectDto1 = new ProjectDto("Test 1");

    final ProjectDto projectDto2 = new ProjectDto("Test 2");

    final ProjectDto projectDto3 = new ProjectDto("Test 3");

    final ProjectDto projectDto4 = new ProjectDto("Test 4");

    @Before
    public void initTest() {
        client.save(projectDto1);
        client.save(projectDto2);
        client.save(projectDto3);
    }

    @After
    public void afterTest() {
        final ProjectRestEndpointClient client = ProjectRestEndpointClient.client();
        client.delete(projectDto1);
        client.delete(projectDto2);
        client.delete(projectDto3);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void add() {
        final String expected = projectDto4.getName();
        final ProjectDto projectDto = client.save(projectDto4);
        Assert.assertNotNull(projectDto);
        final String actual = projectDto.getName();
        Assert.assertEquals(expected,actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        final String id = projectDto1.getId();
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        final List<ProjectDto> projects = client.findAll();
        Assert.assertTrue(projects.stream().anyMatch(x -> Objects.equals(x.getId(), projectDto2.getId())));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        final String id = projectDto2.getId();
        Assert.assertTrue(projectDto2.getId().equals(client.findById(id).getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void count() {
        final long count = client.count();
        Assert.assertTrue(count > 0);
    }

}
