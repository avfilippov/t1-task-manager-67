package ru.t1.avfilippov.tm;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.avfilippov.tm.client.ProjectRestEndpointClient;
import ru.t1.avfilippov.tm.client.TaskRestEndpointClient;
import ru.t1.avfilippov.tm.dto.ProjectDto;
import ru.t1.avfilippov.tm.dto.TaskDto;
import ru.t1.avfilippov.tm.marker.IntegrationCategory;

import java.util.List;
import java.util.Objects;

public class TaskRestEndpointTest {

    final TaskRestEndpointClient client = TaskRestEndpointClient.client();

    final TaskDto taskDto1 = new TaskDto("Test 1");

    final TaskDto taskDto2 = new TaskDto("Test 2");

    final TaskDto taskDto3 = new TaskDto("Test 3");

    final TaskDto taskDto4 = new TaskDto("Test 4");

    final ProjectRestEndpointClient clientProject = ProjectRestEndpointClient.client();

    final ProjectDto projectDto = new ProjectDto("Test Task");


    @Before
    public void initTest() {
        client.save(taskDto1);
        client.save(taskDto2);
        client.save(taskDto3);
        clientProject.save(projectDto);
    }

    @After
    public void afterTest() {
        final TaskRestEndpointClient client = TaskRestEndpointClient.client();
        client.delete(taskDto1);
        client.delete(taskDto2);
        client.delete(taskDto3);
        clientProject.delete(projectDto);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void add() {
        final String expected = taskDto4.getName();
        final TaskDto projectDto = client.save(taskDto4);
        Assert.assertNotNull(projectDto);
        final String actual = projectDto.getName();
        Assert.assertEquals(expected,actual);
    }

    @Test
    @Category(IntegrationCategory.class)
    public void removeById() {
        final String id = taskDto1.getId();
        client.deleteById(id);
        Assert.assertNull(client.findById(id));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findAll() {
        final List<TaskDto> projects = client.findAll();
        Assert.assertTrue(projects.stream().anyMatch(x -> Objects.equals(x.getId(), taskDto2.getId())));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findById() {
        final String id = taskDto2.getId();
        Assert.assertTrue(taskDto2.getId().equals(client.findById(id).getId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void findByProjectId() {
        taskDto2.setProjectId(projectDto.getId());
        Assert.assertNotNull(clientProject.findById(taskDto2.getProjectId()));
    }

    @Test
    @Category(IntegrationCategory.class)
    public void count() {
        final long count = client.count();
        Assert.assertTrue(count > 0);
    }

}
