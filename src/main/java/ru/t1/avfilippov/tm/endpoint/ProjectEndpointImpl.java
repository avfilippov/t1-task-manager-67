package ru.t1.avfilippov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.avfilippov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.avfilippov.tm.api.service.ProjectDTOService;
import ru.t1.avfilippov.tm.dto.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @Autowired
    private ProjectDTOService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<ProjectDto> findAll() {
        return service.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public ProjectDto save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final ProjectDto project) {
        return service.save(project);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        service.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody ProjectDto project
    ) {
        service.delete(project);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody List<ProjectDto> projects
    ) {
        service.deleteAll(projects);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        service.clear();
    }

}
