package ru.t1.avfilippov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.avfilippov.tm.api.endpoint.TaskEndpoint;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.avfilippov.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskDTOService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<TaskDto> findAll() {
        return service.findAll();
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public TaskDto save(
            @WebParam(name = "task", partName = "task")
            @RequestBody final TaskDto task
    ) {
        return service.save(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        return service.existsById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return service.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    ) {
        service.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    ) {
        service.delete(task);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDto> tasks
    ) {
        service.deleteAll(tasks);
    }

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        service.clear();
    }

}
