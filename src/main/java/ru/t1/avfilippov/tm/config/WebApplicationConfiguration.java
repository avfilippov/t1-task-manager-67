package ru.t1.avfilippov.tm.config;

import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import ru.t1.avfilippov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.avfilippov.tm.api.endpoint.TaskEndpoint;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import javax.xml.ws.Endpoint;

@EnableWebMvc
@Configuration
@ComponentScan("ru.t1.avfilippov.tm")
public class WebApplicationConfiguration implements WebMvcConfigurer, WebApplicationInitializer {

    @Bean
    public ViewResolver internalResourceViewResolver() {
        InternalResourceViewResolver bean = new InternalResourceViewResolver();
        bean.setViewClass(JstlView.class);
        bean.setPrefix("/WEB-INF/views/");
        bean.setSuffix(".jsp");
        return bean;
    }

    @Bean
    public SpringBus cxf() {
        return new SpringBus();
    }

    @Bean
    public Endpoint projectEndpointRegistry(final ProjectEndpoint projectEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }


    @Bean
    public Endpoint taskEndpointRegistry(final TaskEndpoint taskEndpoint, SpringBus cxf) {
        final EndpointImpl endpoint = new EndpointImpl(cxf, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }


    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        final CXFServlet cxfServlet = new CXFServlet();
        final ServletRegistration.Dynamic dynamicCXF = servletContext.addServlet("cxfServlet", cxfServlet);
        dynamicCXF.addMapping("/ws/*");
        dynamicCXF.setLoadOnStartup(1);
    }

}
