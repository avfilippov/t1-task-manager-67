package ru.t1.avfilippov.tm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public final class IndexController {

    @GetMapping("/")
    public String index(){
        return "index";
    }

}
