package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.dto.ProjectDto;

public interface ProjectDtoRepository extends JpaRepository<ProjectDto, String> {
}
