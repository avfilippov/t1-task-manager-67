package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.model.Project;

public interface ProjectRepository extends JpaRepository<Project, String> {

}
