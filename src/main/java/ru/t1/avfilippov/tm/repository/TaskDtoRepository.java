package ru.t1.avfilippov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.avfilippov.tm.dto.TaskDto;

public interface TaskDtoRepository extends JpaRepository<TaskDto, String> {
}
