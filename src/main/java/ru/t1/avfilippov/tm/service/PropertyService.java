package ru.t1.avfilippov.tm.service;

import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

        @Value("#{environment['password.iteration']}")
        private Integer passwordIteration;

        @Value("#{environment['password.secret']}")
        private String passwordSecret;

        @Value("#{environment['server.port']}")
        private Integer serverPort;

        @Value("#{environment['server.host']}")
        private String serverHost = "server.host";

        @Value("#{environment['session.key']}")
        private String sessionKey;


        @Value("#{environment['session.timeout']}")
        private Integer sessionTimeout;

        @Value("#{environment['database.username']}")
        private String databaseUser;

        @Value("#{environment['database.password']}")
        private String databasePassword;

        @Value("#{environment['database.url']}")
        private String databaseUrl;

        @Value("#{environment['database.driver']}")
        private String databaseDriver;

        @Value("#{environment['database.dialect']}")
        private String databaseDialect;

        @Value("#{environment['database.show_sql']}")
        private String databaseShowSql;

        @Value("#{environment['database.format_sql']}")
        private String databaseFormatSql;

        @Value("#{environment['database.hbm2ddl_auto']}")
        private String database2ddlAuto;

        @Value("#{environment['database.second_lvl_cache']}")
        private String databaseSecondLvlCache;

        @Value("#{environment['database.factory_class']}")
        private String databaseFactoryClass;

        @Value("#{environment['database.use_query_cache']}")
        private String databaseUseQueryCache;

        @Value("#{environment['database.use_min_puts']}")
        private String databaseUseMinPuts;

        @Value("#{environment['database.region_prefix']}")
        private String databaseRegionPrefix;

        @Value("#{environment['database.config_file_path']}")
        private String databaseConfigFilePath;

        @Value("#{environment['database.default_schema']}")
        private String databaseSchemaName;

        @Value("#{environment['token.init']}")
        private String initToken;

        @Value("#{environment['database.liquibase_config']}")
        private String databaseLiquibaseConfig;

        @Value("#{environment['jms.url']}")
        private String jmsUrl;

        private final Properties properties = new Properties();

        @SneakyThrows
        public PropertyService() {
        }

}
