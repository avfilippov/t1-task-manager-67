package ru.t1.avfilippov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.service.TaskDTOService;
import ru.t1.avfilippov.tm.dto.TaskDto;
import ru.t1.avfilippov.tm.repository.TaskDtoRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TaskDtoServiceImpl implements TaskDTOService {


    @Autowired
    private TaskDtoRepository taskRepository;

    @Override
    public List<TaskDto> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @Transactional
    public TaskDto save(final TaskDto task) {
        return taskRepository.save(task);
    }

    @Override
    public TaskDto save() {
        final TaskDto task = new TaskDto("New name" + LocalDateTime.now().toString());
        return taskRepository.save(task);
    }

    @Override
    public TaskDto findById(final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existsById(final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Override
    @Transactional
    public void deleteById(final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(final TaskDto task) {
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void deleteAll(final List<TaskDto> tasks) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

}
