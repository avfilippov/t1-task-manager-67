package ru.t1.avfilippov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.service.ProjectDTOService;
import ru.t1.avfilippov.tm.dto.ProjectDto;
import ru.t1.avfilippov.tm.repository.ProjectDtoRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class ProjectDtoServiceImpl implements ProjectDTOService {

    @Autowired
    private ProjectDtoRepository projectRepository;

    @Override
    public List<ProjectDto> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public ProjectDto save(final ProjectDto project) {
        return projectRepository.save(project);
    }

    @Override
    public ProjectDto save() {
        final ProjectDto project = new ProjectDto("New name " + LocalDateTime.now().toString());
        return projectRepository.save(project);
    }

    @Override
    public ProjectDto findById(final String id) {
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existsById(final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Override
    public void deleteById(final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    public void delete(final ProjectDto project) {
        projectRepository.delete(project);
    }

    @Override
    public void deleteAll(final List<ProjectDto> projects) {
        projectRepository.deleteAll(projects);
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

}
